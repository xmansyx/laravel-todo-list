<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<title>To-do list</title>
</head>
<body>
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			

			<div class="row">
				
				<h2>to-do List</h2>
				@if(Session::has('addSuccess'))

				<div class="alert alert-success">
					<strong>Success</strong> {{Session::get("addSuccess")}}
				</div>
				@elseif(Session::has('deleteSuccess'))
				<div class="alert alert-success">
					<strong>Success</strong> {{Session::get("deleteSuccess")}}
				</div>
				
				@elseif(Session::has('editSuccess'))

					<div class="alert alert-success">
						<strong>Success</strong> {{Session::get("editSuccess")}}
					</div>
				@endif

				

				@if(count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
						@foreach($errors->all() as $error)

							<li>{{$error}}</li>

						@endforeach
						</ul>
					</div>
				@endif
			</div>

			<div class="row">
				<form action="{{ route("tasks.store") }}" method="POST">
					{{ csrf_field() }}

					<div class="col-md-8">
						<input type="text" name="taskName" class="form-control" id="">
					</div>

					<div class="col-md-4">
						<input type="submit" class="btn btn-primary btn-block" value="submit">
					</div>	

				</form>
		
			</div>
			<div class="row">

				@if(count($tasksData) > 0 )
					<table class="table">
						<thead>
							<th>Tasks {{count($tasksData)}}</th>
							<th>name</th>
							<th>edit</th>
							<th>delete</th>

						</thead>
						<tbody>
							@foreach($tasksData as $taskData)
							<tr>
								<td>{{$taskData->id}}</td>
								<td>{{$taskData->name}}</td>
								<td><a href="{{ route("tasks.edit",["tasks"=>"$taskData->id"]) }}" class="btn btn-default">Edit</a></td>
								<td>
									<form action="{{ route("tasks.destroy",["tasks"=>"$taskData->id"]) }}" method="POST">
										{{ csrf_field()}}
										<input type="hidden" name="_method" value="DELETE" >
										<input type="submit" value="delete" class="btn btn-danger">
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				@endif

			</div>
		</div>
	</div>
</body>
</html>