<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<title>To-do list</title>
</head>
<body>
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			
			<div class="row">
				
				<h2>to-do List</h2>

			</div>

			<div class="row">
				<form action="{{ route("tasks.update",["tasks"=>"$task->id"]) }}" method="POST">

					{{ csrf_field() }}

					<input type="hidden" name="_method" value="PUT">
					<div class="col-md-8">
						<input type="text" name="newTaskName" class="form-control" value="{{ $task->name }}">
					</div>

					<div class="col-md-4">
						<input type="submit" class="btn btn-primary btn-block" value="Edit">
					</div>	

				</form>
		
			</div>

		</div>
	</div>
</body>
</html>